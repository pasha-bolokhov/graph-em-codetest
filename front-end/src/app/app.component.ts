import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

class Hero {
  constructor(
    public name: string,
    public side: number,
    public hitPoints: number,
    public attack: number,
    public specialAbility: string
  ) {}
};

interface HeroInterface {

};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'Hero Store';
  postUrl = 'http://localhost:4300/api/hero/create';
  getUrl = 'http://localhost:4300/api/hero/list';
  sides = [
    {index: 0, value: "dark"},
    {index: 1, value: "light"},
  ];
  heroForm;

  heroes$: Observable<Hero[]>;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient
  ) {
    this.heroForm = this.formBuilder.group({
      name: '',
      side: 1,
      hitPoints: 0,
      attack: 0,
      specialAbility: '',
    });
  }

  ngOnInit() {
    this.fetchHeroes();
  }

  fetchHeroes() {
    this.heroes$ = this.http.get<Hero[]>(this.getUrl);
  }

  onSubmit(value) {
    console.log(`Adding hero:`);
    console.log(value);

    this.addHero(new Hero(value.name, value.side, value.hitPoints, value.attack, value.specialAbility)).subscribe();
    this.fetchHeroes();
  }

  addHero(hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(this.postUrl, hero)
      .pipe(
        catchError((err) => {
          console.log('Error adding hero');
          console.log(hero);
          return of(new Hero('', 1, 0, 0, ''));
        })
      );
  }
}
