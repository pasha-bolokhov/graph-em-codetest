import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  // { path: 'myheroes/createhero', component: CreateHeroComponent },
  // { path: '',   redirectTo: '/myheroes/createhero', pathMatch: 'full' },
  // { path: '**', component: CreateHeroComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
