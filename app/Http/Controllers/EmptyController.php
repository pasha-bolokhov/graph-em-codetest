<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmptyController extends Controller
{
    /**
     * A sample controller
     *
     * @return      string
     */
    public function show()
    {
        return "A sample controller → it works 💖";
    }
}
