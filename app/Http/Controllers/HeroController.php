<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hero;
use Illuminate\Support\Facades\Log;

class HeroController extends Controller
{
    /**
     * Creates a hero
     *
     * @param     Request with hero data
     * @return    void
     */
    public function create(Request $request)
    {
        Log::info('create(): got request ' .
            var_export($request->all(), true));

        $hero = new Hero();
        $hero->name = $request->input('name', '<unnamed hero>');
        $hero->side = $request->input('side', 'light');
        $hero->hitPoints = $request->input('hitPoints', 0);
        $hero->attack = $request->input('attack', 0);
        $hero->specialAbility = $request->input('specialAbility', '');

        $hero->save();
    }

    /**
     * Lists available heroes
     *
     * @return      Hero[]      List of available heroes
     */
    public function list()
    {
        Log::info('list()');

        $heroes = \App\Hero::all();

        Log::info('got heroes ' . var_export($heroes, true));

        return $heroes;
    }
}
